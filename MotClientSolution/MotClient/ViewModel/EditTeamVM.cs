﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MotClient.Model;
using MotClient.Services;

namespace MotClient.ViewModel
{
    class EditTeamVM
    {
        private TeamService _teamService;

        public EditTeamVM()
        {
            _teamService = new TeamService();
        }
        public async Task<Team> UpdateTeam(Team team, string teamName)
        {
            team.Name = teamName;
            return await _teamService.UpdateTeam(team.TeamId, team);
        }
    }
}
