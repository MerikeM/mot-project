﻿using System;
using System.Collections.Generic;
using System.Text;
using BL.DTO;

namespace BL.Services.Interfaces
{
    public interface IParticipationService
    {
        IEnumerable<ParticipationDTO> GetAllParticipations();

        ParticipationDTO GetParticipationById(int id);
        ParticipationDTO AddNewParticipation(ParticipationDTO dto, string userId);
        ParticipationDTO UpdateParticipation(int id, ParticipationDTO dto);
        ParticipationDTO DeleteParticipation(int id, string userId, bool isAdmin);
        IEnumerable<ParticipationDTO> GetAllParticipationsForCompetition(int competitionId);
        IEnumerable<ParticipationDTO> GetAllParticipationsForPerson(int personId);
        IEnumerable<ParticipationDTO> GetAllParticipationsForTeam(int teamId);
    }
}
