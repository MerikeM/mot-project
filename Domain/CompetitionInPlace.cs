﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain
{
    class CompetitionInPlace
    {
        public int CompetitionInPlaceId { get; set; }

        public int CompetitionId { get; set; }
        public Competition Competition { get; set; }

        public int CompetitionPlaceId { get; set; }
        public CompetitionPlace CompetitionPlace { get; set; }
    }
}
