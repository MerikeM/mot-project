﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MotClient.Model;
using MotClient.ViewModel;

namespace MotClient.Views
{
    /// <summary>
    /// Interaction logic for AddNotUser.xaml
    /// </summary>
    public partial class EditUser : Window
    {
        private Person _person;
        private EditUserVM _vm;
        public EditUser(Person person)
        {
            _person = person;
            InitializeComponent();
            this.Loaded += EditUser_Loaded;
        }
        private void EditUser_Loaded(object sender, RoutedEventArgs e)
        {
            _vm = new EditUserVM();
            txtFirstName.Text = _person.FirstName;
            txtLastName.Text = _person.LastName;
            txtPersonalIdCode.Text = _person.PersonalIdCode;


        }



        private async void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            if (txtFirstName.Text == "" || txtLastName.Text == "" || txtPersonalIdCode.Text == "")
            {
                MessageBox.Show("Kõik väljad peavad olema täidetud!", "Viga", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            await _vm.UpdatePerson(_person, txtFirstName.Text, txtLastName.Text, txtPersonalIdCode.Text);
            MyCompetitions newWindow = new MyCompetitions();
            newWindow.Show();
            this.Close();
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            MyCompetitions newWindow = new MyCompetitions();
            newWindow.Show();
            this.Close();
        }
    }
}
