﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MotClient.Model;
using MotClient.Services;

namespace MotClient.ViewModel
{
    class AddCompetitionPlaceVM
    {
        private CompetitionPlaceService _competitionPlaceService;


        public AddCompetitionPlaceVM()
        {
            _competitionPlaceService = new CompetitionPlaceService();
        }
        /// <summary>
        /// Creates competition place using parameters
        /// </summary>
        public async void CreateCompetitionPlace(string name, string address, string county, string comment)
        {
            var place = new CompetitionPlace()
            {
                Name = name,
                Address = address,
                County = county,
                Comment = comment
            };

            await _competitionPlaceService.AddNewCompetitionPlace(place);
        }
    }
}
