﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BL.DTO;
using BL.Factories.Interfaces;
using BL.Services.Interfaces;
using DAL.App.Interfaces;

namespace BL.Services
{
    public class ParticipationService : IParticipationService
    {
        private readonly IAppUnitOfWork _uow;
        private readonly IParticipationFactory _participationFactory;

        public ParticipationService(IAppUnitOfWork uow, IParticipationFactory participationFactory)
        {
            _uow = uow;
            _participationFactory = participationFactory;
        }

        /// <summary>
        /// Gets all participations
        /// </summary>
        /// <returns> IEnumerable of Participations</returns>
        public IEnumerable<ParticipationDTO> GetAllParticipations()
        {
            return _uow.Participations.All()
                .Select(ct => _participationFactory.Create(ct));
        }

        /// <summary>
        /// Gets one specific participation by its id
        /// </summary>
        /// <param name="id">Participation id</param>
        /// <returns>ParticipationDTO type participation</returns>
        public ParticipationDTO GetParticipationById(int id)
        {
            var participation = _uow.Participations.Find(id);
            if (participation == null) return null;

            return _participationFactory.Create(participation);
        }

        /// <summary>
        /// Gets all particiaptions for competition
        /// </summary>
        /// <param name="competitionId">Competitions Id</param>
        /// <returns>List of participationsDTO's</returns>
        public IEnumerable<ParticipationDTO> GetAllParticipationsForCompetition(int competitionId)
        {
            return _uow.Participations.All().Where(p => p.CompetitionId == competitionId).Select(ct => _participationFactory.Create(ct));
        }

        /// <summary>
        /// Gets all particiaptions for person
        /// </summary>
        /// <param name="personId">Person's Id</param>
        /// <returns>List of participationsDTO's</returns>
        public IEnumerable<ParticipationDTO> GetAllParticipationsForPerson(int personId)
        {
            return _uow.Participations.All().Where(p => p.PersonId == personId).Select(ct => _participationFactory.Create(ct));
        }

        /// <summary>
        /// Gets all particiaptions for team
        /// </summary>
        /// <param name="teamId">Team's Id</param>
        /// <returns>List of participationsDTO's</returns>
        public IEnumerable<ParticipationDTO> GetAllParticipationsForTeam(int teamId)
        {
            return _uow.Participations.All().Where(p => p.TeamId == teamId).Select(ct => _participationFactory.Create(ct));
        }

        /// <summary>
        /// Adds new participation
        /// </summary>
        /// <param name="dto">ParticipationDTO type object</param>
        /// <returns>Added participation</returns>
        public ParticipationDTO AddNewParticipation(ParticipationDTO dto, string userId)
        {
            var newParticipation = _participationFactory.Create(dto);
            if (newParticipation == null) return null;

            newParticipation.ApplicationUserId = userId;

            if (!_uow.Competitions.Exists(dto.CompetitionId) ||
                !_uow.People.Exists(dto.PersonId.GetValueOrDefault()) &&
                !_uow.Teams.Exists(dto.TeamId.GetValueOrDefault()))
            {
                return null;
            }

            _uow.Participations.Add(newParticipation);
            _uow.SaveChanges();
            return _participationFactory.Create(newParticipation);
        }

        /// <summary>
        /// Updates one specific participation data
        /// </summary>
        /// <param name="id">Id of Participation to be updated</param>
        /// <param name="dto">New dataof Participation</param>
        /// <returns>Returns updated Participation</returns>
        public ParticipationDTO UpdateParticipation(int id, ParticipationDTO dto)
        {
            var participation = _uow.Participations.Find(id);

            if (participation == null) return null;

            if (!_uow.Competitions.Exists(dto.CompetitionId) ||
                !_uow.People.Exists(dto.PersonId.GetValueOrDefault()) && 
                !_uow.Teams.Exists(dto.TeamId.GetValueOrDefault()))
            {
                return null;
            }

            participation.PersonId = dto.PersonId;
            participation.CompetitionId = dto.CompetitionId;
            participation.Points = dto.Points;
            participation.Time = dto.Time;
            participation.IsDisqualified = dto.IsDisqualified;

            _uow.SaveChanges();

            return _participationFactory.Create(participation);
        }

        /// <summary>
        /// Deletes participation by its id
        /// </summary>
        /// <param name="id">Id of Participation to be deleted</param>
        /// <returns>Returns deleted Participation</returns>
        public ParticipationDTO DeleteParticipation(int id, string userId, bool isAdmin)
        {
            var participation = _uow.Participations.Find(id);
            if (participation == null) return null;

            if (participation.ApplicationUserId != userId && !isAdmin)
            {
                return null;
            }

            _uow.Participations.Remove(participation);
            _uow.SaveChanges();

            return _participationFactory.Create(participation);
        }

    }
}
