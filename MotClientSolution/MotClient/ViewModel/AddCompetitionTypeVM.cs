﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MotClient.Model;
using MotClient.Services;

namespace MotClient.ViewModel
{
    class AddCompetitionTypeVM
    {
        private CompetitionTypeService _competitionTypeService;

        public AddCompetitionTypeVM()
        {
            _competitionTypeService = new CompetitionTypeService();
        }

        /// <summary>
        /// Creates competition place using parameters
        /// </summary>
        public async void CreateCompetitionType(string name, bool hasPoints, bool hasTime, bool isIndividual)
        {
            var type = new CompetitionType()
            {
                Name = name,
                HasPoints = hasPoints,
                HasTime = hasTime,
                IsIndividual = isIndividual
            };

            await _competitionTypeService.AddNewCompetitionType(type);
        }
    }
}
