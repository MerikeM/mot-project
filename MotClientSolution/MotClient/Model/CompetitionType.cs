﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MotClient.Model
{
    public class CompetitionType
    {
        public int CompetitionTypeId { get; set; }

        public string Name { get; set; }

        public bool HasPoints { get; set; }
        public bool HasTime { get; set; }
        public bool IsIndividual { get; set; }

        public List<Competition> Competitions { get; set; } = new List<Competition>();
    }
}
