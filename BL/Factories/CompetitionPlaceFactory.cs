﻿using System;
using System.Collections.Generic;
using System.Text;
using BL.DTO;
using BL.Factories.Interfaces;
using Domain;

namespace BL.Factories
{
    /// <summary>
    /// Creates CompetitionPlaceDTO type object
    /// </summary>
    /// <param name="competitionPlace"> CompetitionPlace type object competitionPlace</param>
    /// <returns>new CompetitionPlaceDTO type object</returns>
    public class CompetitionPlaceFactory : ICompetitionPlaceFactory
    {
        public CompetitionPlaceDTO Create(CompetitionPlace competitionPlace)
        {
            return new CompetitionPlaceDTO()
            {
                CompetitionPlaceId = competitionPlace.CompetitionPlaceId,
                Name = competitionPlace.Name,
                County = competitionPlace.County,
                Address = competitionPlace.Address,
                Comment = competitionPlace.Comment
            };
        }

        /// <summary>
        /// Creates CompetitionPlace type object
        /// </summary>
        /// <param name="dto"> CompetitionPlaceDTO type object</param>
        /// <returns>new CompetitionPlace type object</returns>
        public CompetitionPlace Create(CompetitionPlaceDTO dto)
        {
            return new CompetitionPlace()
            {
                CompetitionPlaceId = dto.CompetitionPlaceId,
                Name = dto.Name,
                County = dto.County,
                Address = dto.Address,
                Comment = dto.Comment
            };
        }
    }
}
