﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BL.DTO
{
    public class PersonInTeamDTO
    {
        [MaxLength(200)]
        public int PersonInTeamId { get; set; }

        [Required]
        public int PersonId { get; set; }
        [Required]
        public int TeamId { get; set; }
        public string TeamName { get; set; }
    }
}
