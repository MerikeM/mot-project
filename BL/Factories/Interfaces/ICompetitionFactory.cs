﻿using System;
using System.Collections.Generic;
using System.Text;
using BL.DTO;
using Domain;

namespace BL.Factories.Interfaces
{
    public interface ICompetitionFactory
    {
        CompetitionDTO Create(Competition c);
        Competition Create(CompetitionDTO dto);
    }
}
