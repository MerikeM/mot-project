﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls.WebParts;
using System.Windows.Controls;
using MotClient.Model;
using MotClient.Services;

namespace MotClient.ViewModel
{
    class RegistrationVM
    {
        private AccountService _accountService;
        private PersonService _personService;

        public RegistrationVM()
        {
            _accountService = new AccountService();
            _personService = new PersonService();
        }

        public async Task<User> CreateUser(string role, string txtUserText, string txtPassword, string txtFirstnameText, string txtLastnameText, string txtPersonalIdCodeText)
        {
            var user = new User()
            {
                email = txtUserText,
                username = txtUserText,
                password = txtPassword,
            };

            if (role == "korraldaja") user.role = "organizer";
            if (role == "osaleja") user.role = "participant";
            var response = await _accountService.Register(user);


            if (role == "osaleja")
            {
                var person = new Person()
                {
                    FirstName = txtFirstnameText,
                    LastName = txtLastnameText,
                    PersonalIdCode = txtPersonalIdCodeText,
                    ApplicationUserId = txtUserText
                };

                _personService.AddNewPerson(person);
            }

            return response;
        }

        public async Task<Person> CheckID(string IdCode)
        {
            var isInDB = await _personService.GetByIdCode(IdCode);

            if (isInDB == null)
            {
                return null;
            }
            return isInDB;
        }
    }
}
