﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MotClient.Model;
using MotClient.Services;

namespace MotClient.ViewModel
{
    public class TeamListVM
    {
        private TeamService _teamService;
        private PersonService _personService;
        private List<PersonInTeam> _personInTeams;

        public List<PersonInTeam> PersonsTeams
        {
            get { return _personInTeams; }
            set { _personInTeams = value; }
        }


        public TeamListVM()
        {
            _teamService = new TeamService();
            _personService = new PersonService();
        }

        public async Task LoadData()
        {
            Person person = await _personService.GetPersonByEmail(StaticValues.Email);
            var teams = await _teamService.GetTeamsByPerson(person.PersonId);
            PersonsTeams = teams;
        }

        public async Task<Team> AddNewTeam(string name)
        {
            Team team = new Team()
            {
                Name = name
            };

            var result = await _teamService.AddNewTeam(team);
            var person = await _personService.GetPersonByEmail(StaticValues.Email);
            if (StaticValues.Role == "participant")
            {
                await _teamService.AddNewPersonInTeam(result.TeamId, person.PersonId);
            }

            return result;
        }

        public async Task<Team> GetTeamById(int teamId)
        {
           return await _teamService.GetById(teamId);
        }
    }
}
