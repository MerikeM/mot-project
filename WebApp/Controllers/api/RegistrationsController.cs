﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BL.DTO;
using BL.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using DAL.App.EF;
using Domain;
using Microsoft.AspNet.Identity;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;

namespace WebApp.Controllers.api
{
    [Produces("application/json")]
    [Route("api/Registrations")]
    public class RegistrationsController : Controller
    {
        private readonly IRegistrationService _registrationService;

        public RegistrationsController(IRegistrationService registrationService)
        {
            _registrationService = registrationService;
        }


        // GET: api/Registrations
        /// <summary>
        /// Get list of registrations
        /// </summary>
        /// <returns>List of registrations</returns>
        /// <response code="200">Returns list of registrations</response>
        [HttpGet]
        public IActionResult Get()
        {
            return Ok(_registrationService.GetAllRegistrations());
        }

        // GET: api/Registrations/5
        /// <summary>
        /// Get registration by id 
        /// </summary>
        /// <param name="id">registration's id</param>
        /// <returns>Registration</returns>
        /// <response code="200">Returns registration</response>
        /// <response code="404">Id doesn't exist</response>
        [HttpGet("{id}", Name = "GetRegistration")]
        public IActionResult Get(int id)
        {
            var registration = _registrationService.GetRegistrationById(id);
            if (registration == null) return NotFound();

            return Ok(registration);
        }

        /// <summary>
        /// Get registration by competitions 
        /// </summary>
        /// <param name="id">competitions's id</param>
        /// <returns>List of Registration</returns>
        /// <response code="200">Returns list of Registration</response>
        /// <response code="404">Id doesn't exist</response>
        [HttpGet("competition/{id}")]
        [AllowAnonymous]
        [Route("competition")]
        public IActionResult GetByCompetition(int id)
        {
            return Ok(_registrationService.GetAllRegistrationsForCompetition(id));
        }

        /// <summary>
        /// Get registration by person 
        /// </summary>
        /// <param name="id">person's id</param>
        /// <returns>List of Registration</returns>
        /// <response code="200">Returns list of Registration</response>
        /// <response code="404">Id doesn't exist</response>
        [HttpGet("person/{id}")]
        [AllowAnonymous]
        [Route("person")]
        public IActionResult GetByPerson(int id)
        {
            return Ok(_registrationService.GetAllRegistrationsForPerson(id));
        }

        /// <summary>
        /// Get registration by teams 
        /// </summary>
        /// <param name="id">team's id</param>
        /// <returns>List of Registration</returns>
        /// <response code="200">Returns list of Registration</response>
        /// <response code="404">Id doesn't exist</response>
        [HttpGet("team/{id}")]
        [AllowAnonymous]
        [Route("team")]
        public IActionResult GetByTeam(int id)
        {
            return Ok(_registrationService.GetAllRegistrationsForTeam(id));
        }

        // POST: api/Registrations
        /// <summary>
        /// Add new registration
        /// </summary>
        /// <param name="registration">Registration</param>
        /// <returns>Added registration</returns>
        /// <response code="201">Returns added registration</response>
        /// <response code="400">Invalid input</response>
        [HttpPost]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult Post([FromBody]RegistrationDTO registration)
        {
            if (!ModelState.IsValid) return BadRequest();

            var newRegistration = _registrationService.AddNewRegistration(registration, User.Identity.GetUserId());
            if (newRegistration == null) return BadRequest();

            return CreatedAtAction("Get", new { id = newRegistration.RegistrationId }, newRegistration);
        }


        // DELETE: api/Registrations/5
        /// <summary>
        /// Delete registration by id
        /// </summary>
        /// <param name="id">Id of registration to be deleted</param>
        /// <returns>Deleted registration</returns>
        /// <response code="200">Returns deleted registration</response>
        /// <response code="400">Id doesn't exist</response>
        [HttpDelete("{id}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult Delete(int id)
        {
            bool isAdminOrOrganizer = User.IsInRole("admin") || User.IsInRole("organizer");
            var deletedRegistration = _registrationService.DeleteRegistration(id, User.Identity.GetUserId(), isAdminOrOrganizer);
            if (deletedRegistration == null) return BadRequest();

            return Ok(deletedRegistration);

        }
    }
}