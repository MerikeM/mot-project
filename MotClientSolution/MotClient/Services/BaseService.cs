﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace MotClient.Services
{
    public class BaseService
    {
        private HttpClient _client;

        public BaseService(string uri)
        {
            _client = new HttpClient();
            _client.BaseAddress = new Uri(uri);
            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", StaticValues.Token);
        }

        public async Task<T> Get<T>(string url)
        {
            var response = await _client.GetAsync(url);
            return await response.Content.ReadAsAsync<T>();
        }

        public async Task<T> Post<T>(string url, T obj)
        {
            var response = await _client.PostAsJsonAsync(url, obj);
            return await response.Content.ReadAsAsync<T>();
        }

        public async Task<T> Delete<T>(string url)
        {
            var response = await _client.DeleteAsync(url);
            return await response.Content.ReadAsAsync<T>();
        }

        public async Task<T> Put<T>(string url, T obj)
        {
            var response = await _client.PutAsJsonAsync(url, obj);
            return await response.Content.ReadAsAsync<T>();
        }
    }
}
