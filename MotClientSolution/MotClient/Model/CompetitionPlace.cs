﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MotClient.Model
{
    public class CompetitionPlace
    {
        public int CompetitionPlaceId { get; set; }

        public string Name { get; set; }

        public string County { get; set; }

        public string Address { get; set; }

        public string Comment { get; set; }

        public List<Competition> Competitions { get; set; } = new List<Competition>();

    }
}
