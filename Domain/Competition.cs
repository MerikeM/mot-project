﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Domain
{
   public class Competition
    {
        public int CompetitionId { get; set; }

        [MaxLength(100)]
        [Required]
        public string Name { get; set; }

        [Required]
        public int CompetitionTypeId { get; set; }

        public CompetitionType CompetitionType { get; set; }

        [Required]
        public int CompetitionPlaceId { get; set; }

        public CompetitionPlace CompetitionPlace { get; set; }

        [Required]
        public DateTime Time { get; set; }

        [MaxLength(1000)]
        public string Comment { get; set; }

        public string ApplicationUserId { get; set; }

        public List<Registration> Registrations { get; set; } = new List<Registration>();
        public List<Participation> Participations { get; set; } = new List<Participation>();
    }
}
