﻿using System;
using System.Collections.Generic;
using System.Text;
using BL.DTO;

namespace BL.Services
{
    public interface IPersonService
    {
        IEnumerable<PersonDTO> GetAllPersons();

        PersonDTO GetPersonById(int id);
        PersonDTO GetPersonByEmail(string email);
        PersonDTO GetPersonByIdCode(string idCode);
        IEnumerable<PersonDTO> GetPeopleByTeam(int id);
        PersonDTO AddNewPerson(PersonDTO dto);
        PersonDTO UpdatePerson(int id, PersonDTO dto, string userId, bool isAdmin);
        PersonDTO DeletePerson(int id, string userId, bool isAdmin);

    }
}
