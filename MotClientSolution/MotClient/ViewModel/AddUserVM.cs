﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MotClient.Model;
using MotClient.Services;

namespace MotClient.ViewModel
{
    public class AddUserVM
    {
        private TeamService _teamService;
        private PersonService _personService;

        public AddUserVM()
        {
            _teamService = new TeamService();
            _personService = new PersonService();
        }

        public async Task<PersonInTeam> AddTeamMember(Team team, string email)
        {
            var p = await _personService.GetPersonByEmail(email);
            if (p == null)
            {
                return null;
            }

            return await _teamService.AddNewPersonInTeam(team.TeamId, p.PersonId);
        }
    }
}

