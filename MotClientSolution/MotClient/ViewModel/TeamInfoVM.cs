﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MotClient.Model;
using MotClient.Services;

namespace MotClient.ViewModel
{
    public class TeamInfoVM
    {
        private PersonService _personService;
        private ParticipationService _participationService;
        private RegistrationService _registrationService;
        private CompetitionService _competitionService;
        public Team Team;

        private List<Registration> _registration;
        private List<Participation> _participation;

        public List<Participation> Participation
        {
            get { return _participation; }
            set
            {
                _participation = value;
            }
        }
        public List<Registration> Registration
        {
            get { return _registration; }
            set
            {
                _registration = value;
            }
        }

        public TeamInfoVM(Team team)
        {
            Team = team;
            _personService = new PersonService();
            _participationService = new ParticipationService();
            _registrationService = new RegistrationService();
            _competitionService = new CompetitionService();
            Participation = new List<Participation>();
            Registration = new List<Registration>();
        }

        public async Task LoadData()
        {
            var allParticipations = await _participationService.GetAllParticipationsForTeam(Team.TeamId);
            var participations = new List<Participation>();
            foreach (var p in allParticipations)
            {
                if (p.Points != 0 || p.Time != TimeSpan.Zero || p.IsDisqualified != false)
                {
                    participations.Add(p);
                }
            }
            Participation = participations;

            var allRegistrations = await _registrationService.GetAllRegistrationsForTeam(Team.TeamId);
            var registrations = new List<Registration>();
            foreach (var r in allRegistrations)
            {
                if (r.CompetitionTime > DateTime.Now)
                {
                    registrations.Add(r);
                }
            }

            Registration = registrations;

        }

        public async Task<Competition> GetCompetitionForRegistration(Registration reg)
        {
            return await _competitionService.GetById(reg.CompetitionId);
        }
    }
}
