﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Domain
{
    public class CompetitionPlace
    {
        public int CompetitionPlaceId { get; set; }

        [MaxLength(100)]
        [Required]
        public string Name { get; set; }

        [Required]
        public County County { get; set; }

        [Required]
        public string Address { get; set; }

        [MaxLength(1000)]
        public string Comment { get; set; }

        public List<Competition> Competitions { get; set; } = new List<Competition>();


    }
}
