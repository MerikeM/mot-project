﻿using System;
using DAL.App.Interfaces.Repositories;
using DAL.Interfaces;
using DAL.Interfaces.Repositories;
using Domain;

namespace DAL.App.Interfaces
{
    public interface IAppUnitOfWork : IUnitOfWork
    {
        ICompetitionTypeRepository CompetitionTypes { get;  }
        IPersonRepository People { get;  }
        ITeamRepository Teams { get; }
        ICompetitionPlaceRepository CompetitionPlaces { get;  }
        ICompetitionRepository Competitions { get; }
        IParticipationRepository Participations { get;  }
        IRegistrationRepository Registrations { get; }
        IPersonInTeamRepository PersonInTeams { get; }
    }
}
