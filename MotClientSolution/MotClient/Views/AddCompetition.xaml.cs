﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MotClient.Model;
using MotClient.ViewModel;

namespace MotClient.Views
{
    /// <summary>
    /// Interaction logic for AddCompetition.xaml
    /// </summary>
    public partial class AddCompetition : Window
    {
        private AddCompetitionVM _vm;

        private bool _editMode;
        private Competition _competitionEdit;

        public AddCompetition()
        {
            _editMode = false;
            InitializeComponent();
            this.Loaded += AddCompetition_Loaded;
        }

        public AddCompetition(Competition competition)
        {
            _editMode = true;
            _competitionEdit = competition;
            InitializeComponent();
            this.Loaded += AddCompetition_Loaded;
        }

        private void AddCompetition_Loaded(object sender, RoutedEventArgs e)
        {
            _vm = new AddCompetitionVM();
            Task.Run(()=>_vm.LoadData()).Wait();
            this.DataContext = _vm;
            if (_editMode)
            {
                btnAdd.Content = "Muuda";
                txtName.Text = _competitionEdit.Name;
                txtComment.Text = _competitionEdit.Comment;

            }
        }

        private async void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            CompetitionType type;
            CompetitionPlace place;
            try
            {
                type = (CompetitionType) cbType.SelectionBoxItem;
                place = (CompetitionPlace) cbPlace.SelectionBoxItem;
            }
            catch (Exception exception)
            {
                MessageBox.Show("Kõik väljad peale kommentaari peavad olema täidetud", "Viga", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            DateTime time;
            try
            {
                time = Convert.ToDateTime(dpDate.SelectedDate);
            }
            catch (Exception exception)
            {
                MessageBox.Show("Kuupäev vales formaadis", "Viga", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }


            if (!_editMode) {
                var add = await _vm.AddCompetition(txtName.Text, type, place, time,
                    txtComment.Text);

                MainWindow mw = new MainWindow();
                mw.Show();
                this.Close();
            }
            else
            {
                var updated = await _vm.UpdateCompetition(txtName.Text, type, place, Convert.ToDateTime(dpDate.SelectedDate),
                    txtComment.Text, _competitionEdit);

                Competitions cw = new Competitions(updated);
                cw.Show();
                this.Close();
            }

        }

        private void btnAddNewType_Click(object sender, RoutedEventArgs e)
        {
            var newAddCompetitionType = new AddCompetitionType();
            newAddCompetitionType.Show();
            this.Close();
            
        }

        private void btnAddNewPlace_Click(object sender, RoutedEventArgs e)
        {
            var newAddCompetitionPlace = new AddCompetitionPlace();
            newAddCompetitionPlace.Show();
            this.Close();
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            if (_editMode)
            {
                Competitions cw = new Competitions(_competitionEdit);
                cw.Show();
                this.Close();
            }
            else
            {
                MainWindow mw = new MainWindow();
                mw.Show();
                this.Close();
            }
        }
    }
}
