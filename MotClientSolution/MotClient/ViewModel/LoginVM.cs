﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using MotClient.Model;
using MotClient.Services;

namespace MotClient.ViewModel
{
    class LoginVM
    {
        private AccountService _accountService;

        public LoginVM()
        {
            _accountService = new AccountService();
        }

        public async Task<string> Login(string txtUserText, string pw)
        {
            User user = new User()
            {
                email = txtUserText,
                password = pw
            };

            var userWithToken = await _accountService.Login(user);
            if (userWithToken != null)
            {
                StaticValues.Token = userWithToken.token;

                var handler = new JwtSecurityTokenHandler();
                var jsonToken = handler.ReadToken(StaticValues.Token) as JwtSecurityToken;
                StaticValues.Role = jsonToken.Payload["http://schemas.microsoft.com/ws/2008/06/identity/claims/role"]
                    .ToString();
                StaticValues.Email = jsonToken.Subject;

                return userWithToken.token;
            }


            return null;
        }
    }
}
