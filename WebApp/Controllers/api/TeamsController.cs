﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BL.DTO;
using BL.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using DAL.App.EF;
using Domain;
using Microsoft.AspNet.Identity;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;

namespace WebApp.Controllers.api
{
    [Produces("application/json")]
    [Route("api/Teams")]
    public class TeamsController : Controller
    {
        private readonly ITeamService _teamService;
        private readonly IPersonInTeamService _personInTeamService;

        public TeamsController(ITeamService teamService, IPersonInTeamService personInTeamService)
        {
            _teamService = teamService;
            _personInTeamService = personInTeamService;
        }


        // GET: api/Teams
        /// <summary>
        /// Get list of teams
        /// </summary>
        /// <returns>List of teams</returns>
        /// <response code="200">Returns list of teams</response>
        [HttpGet]
        public IActionResult Get()
        {
            return Ok(_personInTeamService.GetAllPersonInTeams());
        }

        // GET: api/Teams/5
        /// <summary>
        /// Get team by id 
        /// </summary>
        /// <param name="id">team's id</param>
        /// <returns>Team</returns>
        /// <response code="200">Returns team</response>
        /// <response code="404">Id doesn't exist</response>
        [HttpGet("{id}", Name = "GetTeam")]
        public IActionResult Get(int id)
        {
            var team = _teamService.GetTeamById(id);
            if (team == null) return NotFound();

            return Ok(team);
        }

        /// <summary>
        /// Get teams by person 
        /// </summary>
        /// <param name="id">person's id</param>
        /// <returns>List of personInTeams</returns>
        /// <response code="200">Returns list of personInTeams</response>
        /// <response code="404">Id doesn't exist</response>
        [HttpGet("person/{id}", Name = "GetTeamsByPerson")]
        [Route("person")]
        public IActionResult GetTeamsByPerson(int id)
        {
            var team = _personInTeamService.GetTeamsByPerson(id);
            if (team == null) return NotFound();

            return Ok(team);
        }

        // POST: api/Teams
        /// <summary>
        /// Add new team
        /// </summary>
        /// <param name="team">Team</param>
        /// <returns>Added team</returns>
        /// <response code="201">Returns added team</response>
        /// <response code="400">Invalid input</response>
        [HttpPost]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "admin, participant")]
        public IActionResult Post([FromBody]TeamDTO team)
        {
            if (!ModelState.IsValid) return BadRequest();

            var newTeam = _teamService.AddNewTeam(team);

            return CreatedAtAction("Get", new { id = newTeam.TeamId }, newTeam);
        }

        // POST: api/Teams/1
        /// <summary>
        /// Add new member to the team
        /// </summary>
        /// <param name="teamId">teamId</param>
        /// <param name="personId">personId</param>
        /// <returns>Added member to the team</returns>
        /// <response code="200">Returns added member</response>
        /// <response code="400">Invalid input</response>
        [HttpPost("{teamId}/{personId}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "admin, participant")]
        public IActionResult Post(int teamId, int personId)
        {

            var newTeam = _personInTeamService.AddNewPersonInTeam(teamId, personId, User.Identity.GetUserId());

            return Ok(newTeam);
        }



        // PUT: api/Teams/5
        /// <summary>
        /// Update team info
        /// </summary>
        /// <param name="id">Id of team to be updated</param>
        /// <param name="team">New data of the team</param>
        /// <returns>Updated team</returns>
        /// <response code="200">Returns updated team</response>
        /// <response code="400">Team's data in incorrect format or id doesn't exist</response>
        [HttpPut("{id}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "admin, participant")]
        public IActionResult Put(int id, [FromBody]TeamDTO team)
        {
            if (!ModelState.IsValid) return BadRequest();

            var updatedTeam = _teamService.UpdateTeam(id, team, User.Identity.GetUserId());

            if (updatedTeam == null) return BadRequest();

            return Ok(updatedTeam);
        }

        // DELETE: api/Teams/5
        /// <summary>
        /// Delete team by id
        /// </summary>
        /// <param name="id">Id of team to be deleted</param>
        /// <returns>Deleted team</returns>
        /// <response code="200">Returns deleted team</response>
        /// <response code="400">Id doesn't exist</response>
        [HttpDelete("{id}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "admin")]
        public IActionResult Delete(int id)
        {
            var deletedTeam = _teamService.DeleteTeam(id);
            if (deletedTeam == null) return BadRequest();

            return Ok(deletedTeam);

        }
        // DELETE: api/Teams/5/5
        /// <summary>
        /// Delete person in team by teamsId and personsId
        /// </summary>
        /// <param name="teamId">Id of team which memeber will be deleted</param>
        /// <param name="personId">Id of person who will be deleted from team</param>
        /// <returns>Deleted personInTeam</returns>
        /// <response code="200">Returns deleted personInTeam</response>
        /// <response code="400">Id doesn't exist</response>
        [HttpDelete("{teamId}/{personId}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "admin, participant")]
        public IActionResult DeletePeronInTeam(int teamId, int personId)
        {
            var deletedTeam = _personInTeamService.DeletePersonInTeam(teamId, personId, User.Identity.GetUserId());
            if (deletedTeam == null) return BadRequest();

            return Ok(deletedTeam);

        }
    }
}