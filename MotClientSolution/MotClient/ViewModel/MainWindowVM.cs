﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MotClient.Model;
using MotClient.Services;

namespace MotClient.ViewModel
{
    class MainWindowVM : INotifyPropertyChanged
    {
        private List<Competition> _competitions;
        private List<CompetitionPlace> _competitionPlaces;
        private CompetitionService _competitionService;
        private CompetitionPlaceService _competitionPlaceService;

        protected ObservableCollection<Competition> _searchResults;

        public List<Competition> Competitions
        {
            get { return _competitions; }
            set
            {
                _competitions = value;
                NotifyPropertyChanged("Competitions");
            }
        }

        public List<CompetitionPlace> CompetitionPlaces
        {
            get { return _competitionPlaces; }
            set
            {
                _competitionPlaces = value;
                NotifyPropertyChanged("CompetitionPlaces");
            }
        }

        public ObservableCollection<Competition> SearchResults
        {
            get { return _searchResults; }
            private set
            {
                _searchResults = value;
                NotifyPropertyChanged("SearchResults");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        // This method is called by the Set accessor of each property.
        // The CallerMemberName attribute that is applied to the optional propertyName
        // parameter causes the property name of the caller to be substituted as an argument.
        private void NotifyPropertyChanged(string propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public MainWindowVM()
        {
            _competitionService = new CompetitionService();
            _competitionPlaceService = new CompetitionPlaceService();
            Competitions = new List<Competition>();
            CompetitionPlaces = new List<CompetitionPlace>();

        }

        public async void LoadData()
        {
            Competitions = await _competitionService.GetAllCompetitions();
            CompetitionPlaces = await _competitionPlaceService.GetAllCompetitionPlaces();
        }

        /// <summary>
        /// Search competitions by its time and address 
        /// </summary>
        public void Search(string place, DateTime time)
        {
            var places = CompetitionPlaces.Where(o => o.Address.ToLower().Contains(place.ToLower()))
                                           .Select(x => x.CompetitionPlaceId);
            var competitionsInPlace = Competitions.Where(x => places.Contains(x.CompetitionPlaceId));

            
            if (time != DateTime.MinValue)
            {
                var results = competitionsInPlace.Where(x => x.Time.Date.Equals(time.Date)).ToList();
                _searchResults = new ObservableCollection<Competition>(results);
            }
            else
            {
                var results = competitionsInPlace.ToList();
                _searchResults = new ObservableCollection<Competition>(results);
            }

           
        }
    }
}
