﻿using System;
using System.Collections.Generic;
using System.Text;
using BL.DTO;

namespace BL.Services.Interfaces
{
    public interface IPersonInTeamService
    {
        IEnumerable<PersonInTeamDTO> GetAllPersonInTeams();
        PersonInTeamDTO GetPersonInTeamById(int id);
        IEnumerable<PersonInTeamDTO> GetTeamsByPerson(int id);
        PersonInTeamDTO AddNewPersonInTeam(int teamId, int personId, string userId);
        PersonInTeamDTO DeletePersonInTeam(int teamId, int personId, string userId);
    }
}
