﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BL.DTO
{
    public class RegistrationDTO
    {
        public int RegistrationId { get; set; }
        public int? PersonId { get; set; }
        public int? TeamId { get; set; }
        [Required]
        public int CompetitionId { get; set; }
        public string PersonFirstName { get; set; }
        public string PersonLastName { get; set; }
        public string CompetitionName { get; set; }
        public DateTime CompetitionTime { get; set; }
        public string TeamName { get; set; }
    }
}
