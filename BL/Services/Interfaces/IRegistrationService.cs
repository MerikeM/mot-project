﻿using System;
using System.Collections.Generic;
using System.Text;
using BL.DTO;

namespace BL.Services.Interfaces
{
    public interface IRegistrationService
    {
        IEnumerable<RegistrationDTO> GetAllRegistrations();
        RegistrationDTO GetRegistrationById(int id);
        IEnumerable<RegistrationDTO> GetAllRegistrationsForCompetition(int competitionId);
        IEnumerable<RegistrationDTO> GetAllRegistrationsForPerson(int personId);
        IEnumerable<RegistrationDTO> GetAllRegistrationsForTeam(int teamId);
        RegistrationDTO AddNewRegistration(RegistrationDTO dto, string userId);
        RegistrationDTO DeleteRegistration(int id, string userId, bool isAdmin);
    }
}
