﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MotClient.Model;
using MotClient.Services;

namespace MotClient.ViewModel
{
    public class RegistrateTeamVM
    {
        private TeamService _teamService;
        private PersonService _personService;
        private RegistrationService _registrationService;
        private ParticipationService _participationService;
        private List<PersonInTeam> _personInTeams;

        public List<PersonInTeam> PersonsTeams
        {
            get { return _personInTeams; }
            set { _personInTeams = value; }
        }


        public RegistrateTeamVM()
        {
            _teamService = new TeamService();
            _personService = new PersonService();
            _registrationService = new RegistrationService();
            _participationService = new ParticipationService();
        }
        public async Task LoadData()
        {
            if (StaticValues.Role == "participant")
            {
                Person person = await _personService.GetPersonByEmail(StaticValues.Email);
                var teams = await _teamService.GetTeamsByPerson(person.PersonId);
                PersonsTeams = teams;
            }
            else
            {
                var teams = await _teamService.GetAllTeams();
                List<PersonInTeam> distinctTeam = teams.GroupBy(p => p.TeamId).Select(g => g.First()).ToList();
                PersonsTeams = distinctTeam;
            }
        }

        /// <summary>
        /// Registrates team to competition, if competition has not taken place then it will 
        /// registrate both registration and participation otherwise it registrates only participation
        /// </summary>
        public async Task<Participation> RegistrateTeam(Competition competition, PersonInTeam pit)
        {
            var registrations = await _registrationService.GetAllRegistrationsForCompetition(competition.CompetitionId);
            var reg = registrations.FirstOrDefault(x => x.TeamId == pit.TeamId);
            var participations =
                await _participationService.GetAllParticipationsForCompetition(competition.CompetitionId);
            var part = participations.FirstOrDefault((x => x.TeamId == pit.TeamId));

            if (reg == null)
            {
                if (DateTime.Now < competition.Time)
                {
                    Registration newRegistration = new Registration()
                    {
                        TeamId = pit.TeamId,
                        CompetitionId = competition.CompetitionId
                    };
                    await _registrationService.AddNewRegistration(newRegistration);
                }
            }
            if (part == null)
            {
                Participation newParticipation = new Participation()
                {
                    TeamId = pit.TeamId,
                    CompetitionId = competition.CompetitionId
                };

                var registration = await _participationService.AddNewParticipation(newParticipation);

            return registration;
            }

            return null;
        }
    }
}
