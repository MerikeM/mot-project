﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL.App.Interfaces.Repositories;
using DAL.EF.Repositories;
using Domain;
using Microsoft.EntityFrameworkCore;

namespace DAL.App.EF.Repositories
{
    public class EFCompetitionPlaceRepository : EFRepository<CompetitionPlace>, ICompetitionPlaceRepository
    {
        public EFCompetitionPlaceRepository(DbContext dataContext) : base(dataContext)
        {
        }

        public bool Exists(int id)
        {
            return RepositoryDbSet.Any(e => e.CompetitionPlaceId == id);
        }
    }
}
