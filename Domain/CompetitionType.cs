﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Domain
{
    public class CompetitionType
    {
        public int CompetitionTypeId { get; set; }

        [MaxLength(100)]
        [Required]
        public string Name { get; set; }

        public bool HasPoints { get; set; }
        public bool HasTime { get; set; }

        public bool IsIndividual { get; set; }

        public List<Competition> Competitions { get; set; } = new List<Competition>();
    }
}
