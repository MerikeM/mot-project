﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MotClient.Model;
using MotClient.Services;

namespace MotClient.ViewModel
{
    class RegisterToCompetitionVM
    {
        private RegistrationService _registrationService;
        private PersonService _personService;
        private ParticipationService _participationService;

        public RegisterToCompetitionVM()
        {
            _participationService = new ParticipationService();
            _registrationService = new RegistrationService();
            _personService = new PersonService();
        }

        public async Task<Registration> RegistrateUser(Competition competition)
        {

            var person = await _personService.GetPersonByEmail(StaticValues.Email);
            Registration newRegistration = new Registration()
            {
                PersonId = person.PersonId,
                CompetitionId = competition.CompetitionId
            };

            Participation newParticipation = new Participation()
            {
                PersonId = person.PersonId,
                CompetitionId = competition.CompetitionId
            };


            var registrations = await _registrationService.GetAllRegistrationsForCompetition(competition.CompetitionId);
            var registration = registrations.FirstOrDefault(x => x.PersonId == person.PersonId);

            if (registration == null)
            {
                var register = await _registrationService.AddNewRegistration(newRegistration);
                await _participationService.AddNewParticipation(newParticipation);
                return register;

            }

            return null;
        }

        /// <summary>
        /// Registrates person to competition, if competition has not taken place then it will 
        /// registrate both registration and participation otherwise it registrates only participation
        /// </summary>
        public async Task<Participation> RegisterSomebody(Competition competition, string firstName, string lastName)
        {
            var newPerson = new Person()
            {
                FirstName = firstName,
                LastName = lastName
            };
            var addedPerson = await _personService.AddNewPerson(newPerson);

            if (DateTime.Now < competition.Time)
            {
                Registration newRegistration = new Registration()
                {
                    PersonId = addedPerson.PersonId,
                    CompetitionId = competition.CompetitionId
                };
             await _registrationService.AddNewRegistration(newRegistration);
            }

            Participation newParticipation = new Participation()
            {
                PersonId = addedPerson.PersonId,
                CompetitionId = competition.CompetitionId
            };

            var registration = await _participationService.AddNewParticipation(newParticipation);

            return registration;

        }
    }
}
