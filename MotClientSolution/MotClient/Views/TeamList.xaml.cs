﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MotClient.Model;
using MotClient.ViewModel;

namespace MotClient.Views
{
    /// <summary>
    /// Interaction logic for TeamList.xaml
    /// </summary>
    public partial class TeamList : Window
    {
        private TeamListVM _vm;
        public TeamList()
        {
            InitializeComponent();
            this.Loaded += TeamList_Loaded;
        }
        private void TeamList_Loaded(object sender, RoutedEventArgs e)
        {
            _vm = new TeamListVM();
            Task.Run(() => _vm.LoadData()).Wait();
            this.DataContext = _vm;
        }
    

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            MainWindow newWindow = new MainWindow();
            newWindow.Show();
            this.Close();
        }

        private async void btnRegistrateTeam_Click(object sender, RoutedEventArgs e)
        {
            if (txtName.Text == "")
            {
                MessageBox.Show("Tiimil peab olema nimi", "Viga", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            await _vm.AddNewTeam(txtName.Text);

            TeamList newWindow = new TeamList();
            newWindow.Show();
            this.Close();
        }

        private async void txtTeam_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ClickCount == 2)
            {
                PersonInTeam pit = ((FrameworkElement) sender).DataContext as PersonInTeam;
                Team comp = await _vm.GetTeamById(pit.TeamId);
                TeamInfo newWindow = new TeamInfo(comp);
                newWindow.Show();
                this.Close();
            }
        }
    }
}
