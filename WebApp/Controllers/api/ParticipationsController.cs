﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BL.DTO;
using BL.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using DAL.App.EF;
using Domain;
using Microsoft.AspNet.Identity;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;

namespace WebApp.Controllers.api
{
    [Produces("application/json")]
    [Route("api/Participations")]
    public class ParticipationsController : Controller
    {
        private readonly IParticipationService _participationService;

        public ParticipationsController(IParticipationService participationService)
        {
            _participationService = participationService;
        }


        // GET: api/Participations
        /// <summary>
        /// Get list of participation
        /// </summary>
        /// <returns>List of Participations</returns>
        /// <response code="200">Returns list of Participations</response>
        [HttpGet]
        public IActionResult Get()
        {
            return Ok(_participationService.GetAllParticipations());
        }

        // GET: api/Participations/5
        /// <summary>
        /// Get participation by id 
        /// </summary>
        /// <param name="id">Participation's id</param>
        /// <returns>Participation</returns>
        /// <response code="200">Returns Participation</response>
        /// <response code="404">Id doesn't exist</response>
        [HttpGet("{id}", Name = "GetParticipation")]
        public IActionResult Get(int id)
        {
            var participation = _participationService.GetParticipationById(id);
            if (participation == null) return NotFound();

            return Ok(participation);
        }

        // GET: api/Participations/competition/5
        /// <summary>
        /// Gets participations for competition
        /// </summary>
        /// <param name="id">Competition Id</param>
        /// <response code="200">Returns list of participations</response>
        /// <response code="404">Id doesn't exist</response>
        /// <returns>Returns list of participations</returns>
        [HttpGet("competition/{id}")]
        [AllowAnonymous]
        [Route("competition")]
        public IActionResult GetByCompetition(int id)
        {
            return Ok(_participationService.GetAllParticipationsForCompetition(id));
        }

        /// <summary>
        /// Gets participation for person
        /// </summary>
        /// <param name="id">Person Id</param>
        /// <response code="200">Returns list of participations</response>
        /// <response code="404">Id doesn't exist</response>
        /// <returns>Returns list of participations</returns>
        [HttpGet("person/{id}")]
        [AllowAnonymous]
        [Route("person")]
        public IActionResult GetByPerson(int id)
        {
            return Ok(_participationService.GetAllParticipationsForPerson(id));
        }

        /// <summary>
        /// Gets participations for teams
        /// </summary>
        /// <param name="id">Team Id</param>
        /// <response code="200">Returns list of participations</response>
        /// <response code="404">Id doesn't exist</response>
        /// <returns>Returns list of participations</returns>
        [HttpGet("team/{id}")]
        [AllowAnonymous]
        [Route("team")]
        public IActionResult GetByTeam(int id)
        {
            return Ok(_participationService.GetAllParticipationsForTeam(id));
        }

        // POST: api/Participations
        /// <summary>
        /// Add new participation
        /// </summary>
        /// <param name="participation">Participation</param>
        /// <returns>Added Participation</returns>
        /// <response code="201">Returns added Participation</response>
        /// <response code="400">Invalid input</response>
        [HttpPost]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult Post([FromBody]ParticipationDTO participation)
        {
            if (!ModelState.IsValid) return BadRequest();

            var newParticipation = _participationService.AddNewParticipation(participation, User.Identity.GetUserId());

            if (newParticipation == null) return BadRequest();

            return CreatedAtAction("Get", new { id = newParticipation.ParticipationId }, newParticipation);
        }

        // PUT: api/Participations/5
        /// <summary>
        /// Update participation info
        /// </summary>
        /// <param name="id">Id of Participation to be updated</param>
        /// <param name="participation">New data of the Participation</param>
        /// <returns>UpdatedPparticipation</returns>
        /// <response code="200">Returns updated Participation</response>
        /// <response code="400">Participation's data in incorrect format or id doesn't exist</response>
        [HttpPut("{id}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "admin, organizer")]
        public IActionResult Put(int id, [FromBody]ParticipationDTO participation)
        {
            if (!ModelState.IsValid) return BadRequest();

            var updatedParticipation = _participationService.UpdateParticipation(id, participation);

            if (updatedParticipation == null) return BadRequest();

            return Ok(updatedParticipation);
        }

        // DELETE: api/Participation/5
        /// <summary>
        /// Delete participation by id
        /// </summary>
        /// <param name="id">Id of Participation to be deleted</param>
        /// <returns>Deleted Participation</returns>
        /// <response code="200">Returns deleted Participation</response>
        /// <response code="400">Id doesn't exist</response>
        [HttpDelete("{id}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult Delete(int id)
        {
            bool isAdminOrOrganizer = User.IsInRole("admin") || User.IsInRole("organizer");
            var deletedParticipation = _participationService.DeleteParticipation(id, User.Identity.GetUserId(), isAdminOrOrganizer);
            if (deletedParticipation == null) return BadRequest();

            return Ok(deletedParticipation);

        }

    }
}