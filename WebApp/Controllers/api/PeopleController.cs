﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BL.DTO;
using BL.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using DAL.App.EF;
using Domain;
using Microsoft.AspNet.Identity;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;

namespace WebApp.Controllers.api
{
    [Produces("application/json")]
    [Route("api/People")]
    public class PeopleController : Controller
    {
        private readonly IPersonService _personService;

        public PeopleController(IPersonService personService)
        {
            _personService = personService;
        }

        // GET: api/People
        /// <summary>
        /// Get list of people
        /// </summary>
        /// <returns>List of people</returns>
        /// <response code="200">Returns list of people</response>
        [HttpGet]
        [ProducesResponseType(typeof(List<Person>), 200)]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "admin")]
        public IActionResult Get()
        {
            return Ok(_personService.GetAllPersons());

        }

        // GET: api/People/5
        /// <summary>
        /// Get person by id 
        /// </summary>
        /// <param name="id">person's id</param>
        /// <returns>Person</returns>
        /// <response code="200">Returns person</response>
        /// <response code="404">Id doesn't exist</response>
        [HttpGet("{id}", Name = "GetPerson")]
        [ProducesResponseType(typeof(Person), 200)]
        [ProducesResponseType(404)]
        public IActionResult Get(int id)
        {
            var p = _personService.GetPersonById(id);
            if (p == null) return NotFound();
            return Ok(p);
        }

        /// <summary>
        /// Get person by Email
        /// </summary>
        /// <param name="email">person's email</param>
        /// <returns>Person</returns>
        /// <response code="200">Returns person</response>
        /// <response code="404">Id doesn't exist</response>
        [HttpGet("email/{email}", Name = "GetPersonByEmail")]
        [ProducesResponseType(typeof(Person), 200)]
        [ProducesResponseType(404)]
        [Route("email")]
        public IActionResult GetPersonByEmail(string email)
        {
            var p = _personService.GetPersonByEmail(email);
            if (p == null) return NotFound();
            return Ok(p);
        }

        /// <summary>
        /// Get person by personal IDcode
        /// </summary>
        /// <param name="idCode">person's idCode</param>
        /// <returns>Person</returns>
        /// <response code="200">Returns person</response>
        /// <response code="404">Id doesn't exist</response>
        [HttpGet("idcode/{idCode}", Name = "GetPersonByIdCode")]
        [ProducesResponseType(typeof(Person), 200)]
        [ProducesResponseType(404)]
        [Route("idcode")]
        public IActionResult GetPersonByIdCode(string idCode)
        {
            var p = _personService.GetPersonByIdCode(idCode);
            if (p == null) return NotFound();
            return Ok(p);
        }

        /// <summary>
        /// Get people by team
        /// </summary>
        /// <param name="id">team's id</param>
        /// <returns>Returns List of people</returns>
        /// <response code="200">Returns List of people</response>
        /// <response code="404">Id doesn't exist</response>
        [HttpGet("team/{id}", Name = "GetPeopleByTeam")]
        [ProducesResponseType(typeof(List<Person>), 200)]
        [ProducesResponseType(404)]
        [Route("team")]
        public IActionResult GetPeopleByTeam(int id)
        {
            var p = _personService.GetPeopleByTeam(id);
            if (p == null) return NotFound();
            return Ok(p);
        }

        // POST: api/People
        /// <summary>
        /// Add new person
        /// </summary>
        /// <param name="p">Person</param>
        /// <returns>Added person</returns>
        /// <response code="201">Returns added person</response>
        /// <response code="400">Invalid input</response>
        [HttpPost]
        [ProducesResponseType(typeof(Person), 201)]
        [ProducesResponseType(400)]
        public IActionResult Post([FromBody]PersonDTO p)
        {
            if (!ModelState.IsValid) return BadRequest();

            var newPerson = _personService.AddNewPerson(p);

            return CreatedAtAction("Get", new { id = newPerson.PersonId }, newPerson);

        }

        // PUT: api/People/5
        /// <summary>
        /// Update person info
        /// </summary>
        /// <param name="id">Id of person to be updated</param>
        /// <param name="p">New data of the person</param>
        /// <returns>Updated person</returns>
        /// <response code="200">Returns updated person</response>
        /// <response code="400">Person's data in incorrect format or id doesn't exist</response>
        [HttpPut("{id}")]
        [ProducesResponseType(typeof(Person), 200)]
        [ProducesResponseType(400)]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "admin, participant")]
        public IActionResult Put(int id, [FromBody]PersonDTO p)
        {
            if (!ModelState.IsValid) return BadRequest();

            var updatedPerson = _personService.UpdatePerson(id, p, User.Identity.GetUserId(), User.IsInRole("admin"));

            if (updatedPerson == null) return BadRequest();

            return Ok(updatedPerson);
        }

        // DELETE: api/People/5
        /// <summary>
        /// Delete person by id
        /// </summary>
        /// <param name="id">Id of person to be deleted</param>
        /// <returns>Deleted person</returns>
        /// <response code="200">Returns deleted person</response>
        /// <response code="400">Id doesn't exist</response>
        [HttpDelete("{id}")]
        [ProducesResponseType(typeof(Person), 200)]
        [ProducesResponseType(400)]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "admin")]
        public IActionResult Delete(int id)
        {
            var deletedPerson = _personService.DeletePerson(id, User.Identity.GetUserId(), User.IsInRole("admin"));
            if (deletedPerson == null) return BadRequest();

            return Ok(deletedPerson);
        }

    }
}