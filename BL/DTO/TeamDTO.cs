﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BL.DTO
{
    public class TeamDTO
    {
        public int TeamId { get; set; }

        [MaxLength(100)]
        [Required]
        public string Name { get; set; }
    }
}
