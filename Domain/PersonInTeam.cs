﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Domain
{
    public class PersonInTeam
    {
        public int PersonInTeamId { get; set; }

        [Required]
        public int PersonId { get; set; }

        public Person Person { get; set; }

        [Required]
        public int TeamId { get; set; }

        public Team Team { get; set; }
    }
}
