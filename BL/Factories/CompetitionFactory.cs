﻿using System;
using System.Collections.Generic;
using System.Text;
using BL.DTO;
using BL.Factories.Interfaces;
using Domain;

namespace BL.Factories
{
    public class CompetitionFactory : ICompetitionFactory
    {
        /// <summary>
        /// Creates CompetitionDTO type objcet
        /// </summary>
        /// <param name="c"> Competition type object c</param>
        /// <returns>new CompetitionDTO type objcet</returns>
        public CompetitionDTO Create(Competition c)
        {
            return new CompetitionDTO()
            {
                CompetitionId = c.CompetitionId,
                Name = c.Name,
                CompetitionPlaceId = c.CompetitionPlaceId,
                CompetitionTypeId = c.CompetitionTypeId,
                Time = c.Time,
                Comment = c.Comment,
                ApplicationUserId = c.ApplicationUserId
            };
        }

        /// <summary>
        /// Creates Competition type object
        /// </summary>
        /// <param name="dto"> CompetitionDTO type objcet</param>
        /// <returns>new Competition type objcet</returns>
        public Competition Create(CompetitionDTO dto)
        {
            return new Competition()
            {
                CompetitionId = dto.CompetitionId,
                Name = dto.Name,
                CompetitionPlaceId = dto.CompetitionPlaceId,
                CompetitionTypeId = dto.CompetitionTypeId,
                Time = dto.Time,
                Comment = dto.Comment,
                ApplicationUserId = dto.ApplicationUserId
            };
        }
    }
}
