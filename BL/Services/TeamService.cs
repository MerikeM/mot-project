﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BL.DTO;
using BL.Factories.Interfaces;
using BL.Services.Interfaces;
using DAL.App.Interfaces;

namespace BL.Services
{
    public class TeamService : ITeamService
    {
        private readonly IAppUnitOfWork _uow;
        private readonly ITeamFactory _teamFactory;

        public TeamService(IAppUnitOfWork uow, ITeamFactory teamFactory)
        {
            _uow = uow;
            _teamFactory = teamFactory;
        }


        /// <summary>
        /// Gets one specific team by its id
        /// </summary>
        /// <param name="id">Team id</param>
        /// <returns>TeamDTO type team</returns>
        public TeamDTO GetTeamById(int id)
        {
            var t = _uow.Teams.Find(id);
            if (t == null) return null;

            return _teamFactory.Create(t);
        }

        /// <summary>
        /// Adds new team
        /// </summary>
        /// <param name="dto">TeamDTO type object</param>
        /// <returns>Added team</returns>
        public TeamDTO AddNewTeam(TeamDTO dto)
        {
            var newTeam = _teamFactory.Create(dto);
            if (newTeam == null) return null;

            _uow.Teams.Add(newTeam);
            _uow.SaveChanges();

            return _teamFactory.Create(newTeam);
        }

        /// <summary>
        /// Updates one specific team data
        /// </summary>
        /// <param name="id">Id of Team to be updated</param>
        /// <param name="dto">New dataof Team</param>
        /// <returns>Returns updated Team</returns>
        public TeamDTO UpdateTeam(int teamId, TeamDTO dto, string userId)
        {
            var person = _uow.People.All().FirstOrDefault(p => p.ApplicationUserId == userId);
            var inTeam = _uow.PersonInTeams.All()
                .FirstOrDefault(x => x.PersonId == person.PersonId && x.TeamId == teamId);
            if (inTeam == null)
            {
                return null;
            }

            var t = _uow.Teams.Find(teamId);

            if (t == null) return null;

            t.Name = dto.Name;

            _uow.SaveChanges();

            return _teamFactory.Create(t);
        }

        /// <summary>
        /// Deletes team by its id
        /// </summary>
        /// <param name="id">Id of Team to be deleted</param>
        /// <returns>Returns deleted team</returns>
        public TeamDTO DeleteTeam(int id)
        {
            var t = _uow.Teams.Find(id);
            if (t == null) return null;

            _uow.Teams.Remove(t);
            _uow.SaveChanges();

            return _teamFactory.Create(t);
        }
    }
}
