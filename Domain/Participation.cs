﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Domain
{
    public class Participation
    {
        public int ParticipationId { get; set; }

        public int? PersonId { get; set; }
        public int? TeamId { get; set; }

        public Person Person { get; set; }
        public Team Team { get; set; }

        [Required]
        public int CompetitionId { get; set; }

        public Competition Competition { get; set; }

        public double Points { get; set; }
        public TimeSpan Time { get; set; }
        public bool IsDisqualified { get; set; }
        public string ApplicationUserId { get; set; }
    }
}
