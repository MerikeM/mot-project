﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MotClient.Model;

namespace MotClient.Services
{
    class CompetitionPlaceService : BaseService
    {
        public CompetitionPlaceService() : base(Config.CompetitionPlaceServiceUrl)
        {

        }

        public async Task<List<CompetitionPlace>> GetAllCompetitionPlaces()
        {
            return await base.Get<List<CompetitionPlace>>("");
        }

        public async Task<CompetitionPlace> GetById(int id)
        {
            return await base.Get<CompetitionPlace>("competitionplaces/" + id.ToString());
        }

        public async Task<CompetitionPlace> AddNewCompetitionPlace(CompetitionPlace p)
        {
            return await base.Post("", p);
        }
    }
}

