﻿using System;
using System.Collections.Generic;
using System.Text;
using BL.DTO;
using BL.Factories.Interfaces;
using Domain;

namespace BL.Factories
{
    public class PersonFactory : IPersonFactory
    {
        /// <summary>
        /// Creates PersonDTO type object
        /// </summary>
        /// <param name="p"> Person type object p</param>
        /// <returns>new PersonDTO type object</returns>
        public PersonDTO Create(Person p)
        {
            return new PersonDTO()
            {
                PersonId = p.PersonId,
                FirstName = p.FirstName,
                LastName = p.LastName,
                PersonalIdCode = p.PersonalIdCode,
                ApplicationUserId = p.ApplicationUserId

            };
        }

        /// <summary>
        /// Creates Person type object
        /// </summary>
        /// <param name="dto"> PersonDTO type object</param>
        /// <returns>new Person type object</returns>
        public Person Create(PersonDTO dto)
        {
            return new Person()
            {
                PersonId = dto.PersonId,
                FirstName = dto.FirstName,
                LastName = dto.LastName,
                PersonalIdCode = dto.PersonalIdCode,
                ApplicationUserId = dto.ApplicationUserId
            };
        }
    }
}
