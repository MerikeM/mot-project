﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MotClient.Model;

namespace MotClient.Services
{
    class CompetitionService : BaseService
    {
        public CompetitionService() : base(Config.CompetitionServiceUrl)
        {

        }

        public async Task<List<Competition>> GetAllCompetitions()
        {
            return await base.Get<List<Competition>>("");
        }

        public async Task<Competition> GetById(int id)
        {
            return await base.Get<Competition>("competitions/" + id.ToString());
        }

        public async Task<Competition> AddNewCompetition(Competition p)
        {
            return await base.Post("", p);
        }
        public async Task<Competition> DeleteById(int id)
        {
            return await base.Delete<Competition>("competitions/" + id.ToString());
        }

        public async Task<Competition> UpdateCompetition(int id, Competition c)
        {
            return await base.Put<Competition>("competitions/" + id.ToString(), c);
        }
    }
}
