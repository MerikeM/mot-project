﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MotClient.Model;

namespace MotClient.Services
{
    class RegistrationService : BaseService
    {
        public RegistrationService() : base(Config.RegistrationServiceUrl) { }
        public async Task<List<Registration>> GetAllRegistrations()
        {
        return await base.Get<List<Registration>>("");
        }

        public async Task<Registration> GetById(int id)
        {
        return await base.Get<Registration>("registrations/" + id.ToString());
        }
        public async Task<List<Registration>> GetAllRegistrationsForCompetition(int competitionId)
        {
            return await base.Get<List<Registration>>("registrations/competition/" + competitionId.ToString());
        }
        public async Task<List<Registration>> GetAllRegistrationsForPerson(int personId)
        {
            return await base.Get<List<Registration>>("registrations/person/" + personId.ToString());
        }

        public async Task<List<Registration>> GetAllRegistrationsForTeam(int teamId)
        {
            return await base.Get<List<Registration>>("registrations/team/" + teamId.ToString());
        }

        public async Task<Registration> AddNewRegistration(Registration p)
        {
        return await base.Post("", p);
        }

        public async Task<Registration> DeleteById(int id)
        {
            return await base.Delete<Registration>("registrations/" + id.ToString());
        }


    }
}
