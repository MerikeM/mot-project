﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Xml;

namespace BL.DTO
{
   public class CompetitionTypeDTO
    {
        public int CompetitionTypeId { get; set; }

        [MaxLength(100)]
        [Required]
        public string Name { get; set; }

        public bool HasPoints { get; set; }
        public bool HasTime { get; set; }

        public bool IsIndividual { get; set; }
    }
}
