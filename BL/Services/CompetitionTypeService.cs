﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using BL.DTO;
using BL.Factories;
using BL.Factories.Interfaces;
using DAL.App.Interfaces;

namespace BL.Services
{
    public class CompetitionTypeService : ICompetitionTypeService
    {
        private readonly IAppUnitOfWork _uow;
        private readonly ICompetitionTypeFactory _competitionTypeFactory;

        public CompetitionTypeService(IAppUnitOfWork uow, ICompetitionTypeFactory competitionTypeFactory)
        {
            _uow = uow;
            _competitionTypeFactory = competitionTypeFactory;
        }

        /// <summary>
        /// Gets all competition types
        /// </summary>
        /// <returns> IEnumerable of CompetitionTypes</returns>
        public IEnumerable<CompetitionTypeDTO> GetAllCompetitionTypes()
        {
            return _uow.CompetitionTypes.All()
                .Select(ct => _competitionTypeFactory.Create(ct));
        }

        /// <summary>
        /// Gets one specific competition type by its id
        /// </summary>
        /// <param name="id">CompetitionType id</param>
        /// <returns>CompetitionTypeDTO type competition type</returns>
        public CompetitionTypeDTO GetCompetitionTypeById(int id)
        {
            var competitionType = _uow.CompetitionTypes.Find(id);
            if (competitionType == null) return null;

            return _competitionTypeFactory.Create(competitionType);
        }

        /// <summary>
        /// Adds new competition type
        /// </summary>
        /// <param name="dto">CompetitionTypeDTO type object</param>
        /// <returns>Added competition type</returns>
        public CompetitionTypeDTO AddNewCompetitionType(CompetitionTypeDTO dto)
        {
            var newCompetitionType = _competitionTypeFactory.Create(dto);
            _uow.CompetitionTypes.Add(newCompetitionType);
            _uow.SaveChanges();
            return _competitionTypeFactory.Create(newCompetitionType);
        }

        /// <summary>
        /// Updates one specific competition type data
        /// </summary>
        /// <param name="id">Id of CompetitionType to be updated</param>
        /// <param name="dto">New dataof CompetitionType</param>
        /// <returns>Returns updated CompetitionType</returns>
        public CompetitionTypeDTO UpdateCompetitionType(int id, CompetitionTypeDTO dto)
        {
            var competitionType = _uow.CompetitionTypes.Find(id);

            if (competitionType == null) return null;

            competitionType.Name = dto.Name;
            competitionType.HasPoints = dto.HasPoints;
            competitionType.HasTime = dto.HasTime;
            competitionType.IsIndividual = dto.IsIndividual;
            
            _uow.SaveChanges();

            return _competitionTypeFactory.Create(competitionType);
        }

        /// <summary>
        /// Deletes competition type by its id
        /// </summary>
        /// <param name="id">Id of CompetitionType to be deleted</param>
        /// <returns>Returns deleted competition type</returns>
        public CompetitionTypeDTO DeleteCompetitionType(int id)
        {
            var competitionType = _uow.CompetitionTypes.Find(id);
            if (competitionType == null) return null;

            _uow.CompetitionTypes.Remove(competitionType);
            _uow.SaveChanges();

            return _competitionTypeFactory.Create(competitionType);
        }
    }
}
