﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MotClient.Model;
using MotClient.Services;

namespace MotClient.ViewModel
{
    class CompetitionVM

    {
        private CompetitionTypeService _competitionTypeService;
        private CompetitionPlaceService _competitionPlaceService;
        private RegistrationService _registrationService;
        private PersonService _personService;
        private CompetitionService _competitionService;
        private ParticipationService _participationService;

        public CompetitionType CompetitionType;
        public CompetitionPlace CompetitionPlace;
        
        private List<Registration> _registratedPeople;
        private List<Participation> _participants;

        public List<Registration> RegistratedPeople
        {
            get { return _registratedPeople; }
            set
            {
                _registratedPeople = value;
            }
        }
        public List<Participation> Participants
        {
            get { return _participants; }
            set
            {
                _participants = value;
            }
        }



        public CompetitionVM()
        {
            RegistratedPeople = new List<Registration>();
            Participants = new List<Participation>();

            _competitionTypeService = new CompetitionTypeService();
            _competitionPlaceService = new CompetitionPlaceService();
            _registrationService = new RegistrationService();
            _personService = new PersonService();
            _competitionService = new CompetitionService();
            _participationService = new ParticipationService();
        }


        public async Task LoadData(Competition competition)
        {
            CompetitionType = await _competitionTypeService.GetById(competition.CompetitionTypeId);
            CompetitionPlace = await _competitionPlaceService.GetById(competition.CompetitionPlaceId);

            if (DateTime.Now < competition.Time)
            {
                var registrations = await _registrationService.GetAllRegistrationsForCompetition(competition.CompetitionId);
                RegistratedPeople = registrations;
            }
            else
            {
                var participations = await _participationService.GetAllParticipationsForCompetition(competition.CompetitionId);
                Participants = participations;
            }

        }

        /// <summary>
        /// Deletes registration with associated participation
        /// </summary>   
        public async Task<Registration> DeleteRegistration(Competition competition, Registration person)
        {
            if (StaticValues.Role == "organizer" || StaticValues.Role == "admin")
            {
                var registrations = await _registrationService.GetAllRegistrationsForCompetition(competition.CompetitionId);
                var registration = registrations.FirstOrDefault(x => x.PersonId == person.PersonId);

                var participations = await _participationService.GetAllParticipationsForCompetition(competition.CompetitionId);
                var participation = participations.FirstOrDefault(x => x.PersonId == person.PersonId);
                if (registration != null && participation != null)
                {
                    var deletedRegistration = await _registrationService.DeleteById(registration.RegistrationId);
                    await _participationService.DeleteById(participation.ParticipationId);
                    return deletedRegistration;
                }
            }

            if (StaticValues.Role == "participant")
            {
                var p = await _personService.GetPersonByEmail(StaticValues.Email);

                if (p != null)
                {
                    var registrations = await _registrationService.GetAllRegistrationsForCompetition(competition.CompetitionId);
                    var registration = registrations.FirstOrDefault(x => x.PersonId == p.PersonId);

                    var participations = await _participationService.GetAllParticipationsForCompetition(competition.CompetitionId);
                    var participation = participations.FirstOrDefault(x => x.PersonId == p.PersonId);

                    if (registration != null && participation != null)
                    {
                        var deletedRegistration = await _registrationService.DeleteById(registration.RegistrationId);
                        await _participationService.DeleteById(participation.ParticipationId);
                        return deletedRegistration;
                    }
                }
            }

            return null;

        }
        public async Task<Competition> DeleteCompetition(Competition competition)
        {
            if (StaticValues.Email == competition.ApplicationUserId || StaticValues.Role == "admin")
            {


                 return await _competitionService.DeleteById(competition.CompetitionId);
            }

            return null;
        }
    }
}
