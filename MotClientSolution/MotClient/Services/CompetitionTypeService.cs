﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MotClient.Model;

namespace MotClient.Services
{
    class CompetitionTypeService : BaseService
    {

        public CompetitionTypeService() : base(Config.CompetitionTypeServiceUrl)
        {

        }
        public async Task<List<CompetitionType>> GetAllCompetitionTypes()
        {
            return await base.Get<List<CompetitionType>>("");
        }

        public async Task<CompetitionType> GetById(int id)
        {
            return await base.Get<CompetitionType>("competitiontypes/" + id.ToString());
        }

        public async Task<CompetitionType> AddNewCompetitionType(CompetitionType p)
        {
            return await base.Post("", p);
        }
    }
}
