﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BL.DTO;
using BL.Factories.Interfaces;
using BL.Services.Interfaces;
using DAL.App.Interfaces;

namespace BL.Services
{
    public class CompetitionService : ICompetitionService
    {
        private readonly IAppUnitOfWork _uow;
        private readonly ICompetitionFactory _competitionFactory;

        public CompetitionService(ICompetitionFactory competitionFactory, IAppUnitOfWork uow)
        {
            _competitionFactory = competitionFactory;
            _uow = uow;
        }

        /// <summary>
        /// Gets all competitions 
        /// </summary>
        /// <returns> IEnumerable of Competitions</returns>
        public IEnumerable<CompetitionDTO> GetAllCompetitions()
        {
            return _uow.Competitions.All().Select(c => _competitionFactory.Create(c));
        }

        /// <summary>
        /// Gets one specific competition  by its id
        /// </summary>
        /// <param name="id">Competition id</param>
        /// <returns>CompetitionDTO type competition</returns>
        public CompetitionDTO GetCompetitionById(int id)
        {
            var c = _uow.Competitions.Find(id);
            if (c == null) return null;

            return _competitionFactory.Create(c);
        }

        /// <summary>
        /// Adds new competition 
        /// </summary>
        /// <param name="dto">CompetitionDTO type object</param>
        /// <param name="userId">Competition creater user id</param>
        /// <returns>Added competition</returns>
        public CompetitionDTO AddNewCompetition(CompetitionDTO dto, string userId)
        {
            var newCompetition = _competitionFactory.Create(dto);
            if (newCompetition == null) return null;

            newCompetition.ApplicationUserId = userId;

            if (!_uow.CompetitionTypes.Exists(dto.CompetitionTypeId) ||
                !_uow.CompetitionPlaces.Exists(dto.CompetitionPlaceId))
            {
                return null;
            }


            _uow.Competitions.Add(newCompetition);
            _uow.SaveChanges();

            return _competitionFactory.Create(newCompetition);
        }

        /// <summary>
        /// Updates one specific competition data
        /// </summary>
        /// <param name="id">Id of Competition to be updated</param>
        /// <param name="dto">New dataof CompetitionPlace</param>
        /// <param name="userId">Competition updater user id</param>
        /// <param name="isAdmin">Competition updater admin id</param>
        /// <returns>Returns updated Competition</returns>
        public CompetitionDTO UpdateCompetition(int id, CompetitionDTO dto, string userId, bool isAdmin)
        {
            var c = _uow.Competitions.Find(id);

            if (c == null) return null;

            if (!_uow.CompetitionTypes.Exists(dto.CompetitionTypeId) ||
                !_uow.CompetitionPlaces.Exists(dto.CompetitionPlaceId))
            {
                return null;
            }

            if (c.ApplicationUserId != userId && !isAdmin)
            {
                return null;
            }

            c.Name = dto.Name;
            c.Time = dto.Time;
            c.Comment = dto.Comment;
            c.CompetitionPlaceId = dto.CompetitionPlaceId;
            c.CompetitionTypeId = dto.CompetitionTypeId;

            c.ApplicationUserId = userId;

            _uow.SaveChanges();

            return _competitionFactory.Create(c);
        }

        /// <summary>
        /// Deletes competition by its id
        /// </summary>
        /// <param name="id">Id of Competition to be deleted</param>
        /// <param name="userId">Competition deleter user id</param>
        /// <param name="isAdmin">Competition deleter admin id</param>
        /// <returns>Returns deleted competition</returns>
        public CompetitionDTO DeleteCompetition(int id, string userId, bool isAdmin)
        {
            var c = _uow.Competitions.Find(id);
            if (c == null) return null;

            if (c.ApplicationUserId != userId && !isAdmin)
            {
                return null;
            }

            _uow.Competitions.Remove(c);
            _uow.SaveChanges();

            return _competitionFactory.Create(c);
        }
    }
}
