﻿using System;
using System.Collections.Generic;
using System.Text;
using BL.DTO;
using Domain;

namespace BL.Factories.Interfaces
{
    public interface ICompetitionTypeFactory
    {
        CompetitionTypeDTO Create(CompetitionType competitionPlace);
        CompetitionType Create(CompetitionTypeDTO dto);

    }
}
