﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MotClient.Model;
using MotClient.ViewModel;

namespace MotClient.Views
{
    /// <summary>
    /// Interaction logic for TeamInfo.xaml
    /// </summary>
    public partial class TeamInfo : Window
    {

        public Team Team;

        private TeamInfoVM _vm;
        public TeamInfo(Team team)
        {
            Team = team;
            InitializeComponent();
            this.Loaded += TeamInfo_Loaded;
        }
        private void TeamInfo_Loaded(object sender, RoutedEventArgs e)
        {
            _vm = new TeamInfoVM(Team);
            Task.Run(() => _vm.LoadData()).Wait();
            this.DataContext = _vm;

            lblName.Content = Team.Name;

        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            TeamList newWindow = new TeamList();
            newWindow.Show();
            this.Close();
        }

        private async void txtRegistrations_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ClickCount == 2)
            {
                Registration reg = ((FrameworkElement)sender).DataContext as Registration;
                Competition comp = await _vm.GetCompetitionForRegistration(reg);
                Competitions competitions = new Competitions(comp);
                competitions.Show();
                this.Close();
            }
        }

        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            EditTeam newWindow = new EditTeam(Team);
            newWindow.Show();
            this.Close();
        }

        private void btnLookUsers_Click(object sender, RoutedEventArgs e)
        {
            Members newWindow = new Members(Team);
            newWindow.Show();
            this.Close();
        }
    }
}
