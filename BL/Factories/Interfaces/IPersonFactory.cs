﻿using System;
using System.Collections.Generic;
using System.Text;
using BL.DTO;
using Domain;

namespace BL.Factories.Interfaces
{
    public interface IPersonFactory
    {
        PersonDTO Create(Person p);
        Person Create(PersonDTO dto);
    }
}
