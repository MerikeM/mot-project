﻿using System;
using System.Collections.Generic;
using System.Text;
using BL.DTO;
using Domain;

namespace BL.Factories.Interfaces
{
    public interface ITeamFactory
    {
        TeamDTO Create(Team t);
        Team Create(TeamDTO dto);
    }
}
