﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MotClient.Model
{
    public class Registration
    {
        public int RegistrationId { get; set; }

        public int? PersonId { get; set; }
        public int? TeamId { get; set; }
        public Person Person { get; set; }
        public Team Team { get; set; }
        public int CompetitionId { get; set; }
        public Competition Competition { get; set; }
        public string ApplicationUserId { get; set; }
        public string PersonFirstName { get; set; }
        public string PersonLastName { get; set; }
        public string CompetitionName { get; set; }
        public DateTime CompetitionTime { get; set; }
        public string TeamName { get; set; }
    }
}
