﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BL.DTO;
using BL.Factories.Interfaces;
using BL.Services.Interfaces;
using DAL.App.Interfaces;
using Domain;

namespace BL.Services
{
    public class PersonInTeamService: IPersonInTeamService
    {
        private readonly IAppUnitOfWork _uow;
        private readonly IPersonInTeamFactory _personInTeamFactory;

        public PersonInTeamService(IAppUnitOfWork uow, IPersonInTeamFactory personInTeamFactory)
        {
            _uow = uow;
            _personInTeamFactory = personInTeamFactory;
        }

        /// <summary>
        /// Gets all people in team
        /// </summary>
        /// <returns> IEnumerable of PersonInTeam</returns>
        public IEnumerable<PersonInTeamDTO> GetAllPersonInTeams()
        {
            return _uow.PersonInTeams.All().Select(pit => _personInTeamFactory.Create(pit));
        }


        /// <summary>
        /// Gets one specific person in team by its id
        /// </summary>
        /// <param name="id">PersonInTeam id</param>
        /// <returns>PersonInTeamDTO type person in team</returns>
        public PersonInTeamDTO GetPersonInTeamById(int id)
        {
            var r = _uow.PersonInTeams.Find(id);
            if (r == null) return null;

            return _personInTeamFactory.Create(r);
        }

        /// <summary>
        /// Gets teams by person
        /// </summary>
        /// <param name="id">Person's id</param>
        /// <returns>List of PersonInTeamDTO's</returns>
        public IEnumerable<PersonInTeamDTO> GetTeamsByPerson(int id)
        {
            var pit = _uow.PersonInTeams.All().Where(p => p.PersonId == id).Select(t => _personInTeamFactory.Create(t));
            return pit;
        }

        /// <summary>
        /// Adds new person in team
        /// </summary>
        /// <param name="teamId">teamId</param>
        /// <param name="personId">peronId</param>
        /// <returns>Added person in team</returns>
        public PersonInTeamDTO AddNewPersonInTeam(int teamId, int personId, string userId)
        {
            var teamMembers = _uow.PersonInTeams.All().Where(x => x.TeamId == teamId).ToList();

            var person = _uow.People.All().FirstOrDefault(p => p.ApplicationUserId == userId);
            var inTeam = _uow.PersonInTeams.All()
                .FirstOrDefault(x => x.PersonId == person.PersonId && x.TeamId == teamId);
            if (inTeam == null && teamMembers.Count != 0)
            {
                return null;
            }

            if (!_uow.Teams.Exists(teamId) || !_uow.People.Exists(personId))
            {
                return null;
            }

            var addedPersonInTeam = _uow.PersonInTeams.All()
                .FirstOrDefault(x => x.PersonId == personId && x.TeamId == teamId);

            if (addedPersonInTeam != null) return null;

            var newPersonInTeam = new PersonInTeam()
            {
                PersonId = personId,
                TeamId = teamId
            };
            
            _uow.PersonInTeams.Add(newPersonInTeam);
            _uow.SaveChanges();

            return _personInTeamFactory.Create(newPersonInTeam);
        }

        /// <summary>
        /// Deletes person in team by its id
        /// </summary>
        /// <param name="teamId">Id of Team which member will be deleted</param>
        /// <param name="personId">Id of PersonInTeam to be deleted</param>
        /// <param name="userId">Email of currently logged in user</param>
        /// <returns>Returns deleted person in team</returns>
        public PersonInTeamDTO DeletePersonInTeam(int teamId, int personId, string userId)
        {
            var person = _uow.People.All().FirstOrDefault(p => p.ApplicationUserId == userId);
            var inTeam = _uow.PersonInTeams.All()
                .FirstOrDefault(x => x.PersonId == person.PersonId && x.TeamId == teamId);
            if (inTeam == null)
            {
                return null;
            }
            var pit = _uow.PersonInTeams.All().FirstOrDefault(p => p.TeamId == teamId && p.PersonId == personId);

            if (pit == null) return null;

            _uow.PersonInTeams.Remove(pit);
            _uow.SaveChanges();

            return _personInTeamFactory.Create(pit);
        }
    }
}