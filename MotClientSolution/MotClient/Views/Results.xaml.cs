﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MotClient.Model;
using MotClient.ViewModel;

namespace MotClient.Views
{
    /// <summary>
    /// Interaction logic for Results.xaml
    /// </summary>
    public partial class Results : Window
    {
        private Competition _competition;


        private ResultsVM _vm;

        public Results(Competition competition)
        {
            _competition = competition;

            InitializeComponent();
            this.Loaded += Results_Loaded;

        }

        private void Results_Loaded(object sender, RoutedEventArgs e)
        {
            _vm = new ResultsVM();
            Task.Run(() => _vm.LoadData(_competition)).Wait();
            this.DataContext = _vm;

            if (_competition.CompetitionType.IsIndividual)
            {
                lboxParticipants.Columns[2].Visibility = Visibility.Collapsed;
            }
            else
            {
                lboxParticipants.Columns[0].Visibility = Visibility.Collapsed;
                lboxParticipants.Columns[1].Visibility = Visibility.Collapsed;
            }

            if (!_competition.CompetitionType.HasPoints)
            {
                lblPoints.Visibility = Visibility.Hidden;
                txtPoints.Visibility = Visibility.Hidden;
                lboxParticipants.Columns[4].Visibility = Visibility.Collapsed;
            }

            if (!_competition.CompetitionType.HasTime)
            {
                lblTime.Visibility = Visibility.Hidden;
                lblH.Visibility = Visibility.Hidden;
                lblMin.Visibility = Visibility.Hidden;
                lblSec.Visibility = Visibility.Hidden;
                txtHours.Visibility = Visibility.Hidden;
                txtMinutes.Visibility = Visibility.Hidden;
                txtSeconds.Visibility = Visibility.Hidden;
                lboxParticipants.Columns[3].Visibility = Visibility.Collapsed;
            }
        }

        private async void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            var person = lboxParticipants.SelectedItem as Participation;
            if (person == null)
            {
                MessageBox.Show("Valige osaleja, kellele tulemust lisada");
                return;
            }
            var result = await _vm.AddResult(person, _competition, txtPoints.Text, txtHours.Text, txtMinutes.Text, txtSeconds.Text, cbIsDisqualified.IsChecked.GetValueOrDefault());
            if (result != null)
            {
                MessageBox.Show("Tulemus lisatud");
                txtPoints.Clear();
                txtHours.Clear();
                txtMinutes.Clear();
                txtSeconds.Clear();
                cbIsDisqualified.IsChecked = false;
            }
            else
            {
                MessageBox.Show("Viga tulemuse sisestamisel");
            }
            Results newResults = new Results(_competition);
            newResults.Show();
            this.Close();
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            Competitions newCompetition = new Competitions(_competition);
            newCompetition.Show();
            this.Close();
        }

        private void btnAddParticipant_Click(object sender, RoutedEventArgs e)
        {
            if (_competition.CompetitionType.IsIndividual)
            {
                RegisterToCompetition newWindow = new RegisterToCompetition(_competition);
                newWindow.Show();
                this.Close();
            }
            else
            {
                RegistrateTeam newWindow = new RegistrateTeam(_competition);
                newWindow.Show();
                this.Close();
            }
        }

        private async void btnDeleteParticipant_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Soovid kindlasti osalejat kustutada?", "Osaleja kustutamine?",
                    MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
            {
                Participation person = lboxParticipants.SelectedItem as Participation;

                var participation = await _vm.DeleteParticipation(_competition, person);
                if (participation == null)
                {
                    MessageBox.Show("Kustutamine ebaõnnestus");
                    return;
                }

                var newResult = new Results(_competition);
                newResult.Show();
                this.Close();
            }
        }
    }
}
