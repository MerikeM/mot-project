﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MotClient
{
    public static class Config
    {
        private static string getValue(string key)
        {
            return ConfigurationManager.AppSettings[key];
        }

        public static string CompetitionServiceUrl
        {
            get
            {
                return getValue("CompetitionServiceUrl");
            }
        }


        public static string CompetitionPlaceServiceUrl
        {
            get
            {
                return getValue("CompetitionPlaceServiceUrl");
            }
        }

        public static string CompetitionTypeServiceUrl
        {
            get
            {
                return getValue("CompetitionTypeServiceUrl");
            }
        }

        public static string AccountServiceUrl
        {
            get
            {
                return getValue("AccountServiceUrl");
            }
        }

        public static string PersonServiceUrl
        {
            get
            {
                return getValue("PersonServiceUrl");
            }
        }

        public static string RegistrationServiceUrl
        {
            get
            {
                return getValue("RegistrationServiceUrl");
            }
        }


        public static string ParticipationServiceUrl
        {
            get
            {
                return getValue("ParticipationServiceUrl");
            }
        }
        public static string TeamServiceUrl
        {
            get
            {
                return getValue("TeamServiceUrl");
            }
        }

    }
}
