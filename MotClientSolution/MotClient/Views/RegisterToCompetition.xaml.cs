﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MotClient.Model;
using MotClient.ViewModel;

namespace MotClient.Views
{
    /// <summary>
    /// Interaction logic for RegisterToCompetition.xaml
    /// </summary>
    public partial class RegisterToCompetition : Window
    {
        private Competition _competition;

        private RegisterToCompetitionVM _vm;


        public RegisterToCompetition(Competition competition)
        {
            _vm = new RegisterToCompetitionVM();
            _competition = competition;
            InitializeComponent();
            if (StaticValues.Role == "organizer" || StaticValues.Role == "admin" )
            {
                btnRegisterMe.Visibility = Visibility.Hidden;
            }
        }


        private async void btnRegistrateMe_Click(object sender, RoutedEventArgs e)
        {
            var registration = await _vm.RegistrateUser(_competition);
            if (registration == null)
            {
                MessageBox.Show("Olete juba registreerinud");
            }
            else
            {
                var competition = new Competitions(_competition);
                competition.Show();
                this.Close();
            }


        }

        private async void btnRegistrateSomebody_Click(object sender, RoutedEventArgs e)
        {
            if ( txtFirstName.Text == "" || txtLastName.Text == "")
            {
                MessageBox.Show("Kõik väljad peavad olema täidetud!", "Viga", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            var registration = await _vm.RegisterSomebody(_competition, txtFirstName.Text, txtLastName.Text);
            if (DateTime.Now < _competition.Time)
            {
                var competition = new Competitions(_competition);
                competition.Show();
                this.Close();
            }
            else
            {
                var results = new Results(_competition);
                results.Show();
                this.Close();
            }
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            if (DateTime.Now < _competition.Time)
            {
                Competitions newWindow = new Competitions(_competition);
                newWindow.Show();
                this.Close();
            }
            else
            {
                var results = new Results(_competition);
                results.Show();
                this.Close();
            }
        }
    }
}
